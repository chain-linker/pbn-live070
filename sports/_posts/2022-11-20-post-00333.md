---
layout: post
title: "미드영어 크라우디드 시즌1 3화 run out the clock"
toc: true
---

 미드영어 크라우디드 시즌1 3화 run out the clock

◎ Can I just run out the clock until he dies?
 ◆ run out the clock : 축구나 농구 경기에서 연화 끌기해서 이기려는 전략 있죠. 그걸 말하는 겁니다. 딱히 뭘 족다리 않고 시간만 보내고 버티면 이기는 거죠. 이걸 비유로 가져다 쓴 겁니다. kill the clock이라고도 합니다. 귀루 죽이기. :-)

◎ I think you can handle the truth.
 ◆ handle : 우리는 자동차 '핸들'로 담뿍이 쓰죠. 미국인들은 치아 핸들을 동사로 '처리하다, 감당하다'의 뜻으로 모친 씁니다.
 미드 뉴걸 시즌5 1화 I can handle it.
미드 슈퍼걸 시즌1 9화 I will handle it.
미드 모던패밀리 시즌1 12화 I will handle this.
미드 모던패밀리 시즌1 14화 I'll handle it.
미드 더한층 미들 시즌7 13화 I can handle it.
미드 블랙키시 시즌2 19화 How do we not handle this?
http://hi007.tistory.com/2058
 뒤에 또한 나오네요. ◎ your generation can't handle losing.
또 나오고요. ◎ I know how to handle him.

◎ We were just trying to bolster your self-esteem.
 ◆ bolster [뽀울스터] 기운을 복돋우다, 지지하다, 강화하다.

◎ Stella, we're gonna win tonight, on our own, fair and square.
 ◆ fair and square : 정정당당히
미드 브루클린 나인나인 시즌2 15화 Yon won fair and square.
http://hi007.tistory.com/2250

◎ Little Shea with the coke bottle glasses, acne, and dental headgear?
 ◆ acne [애크니] 여드름

◎ Yes, puberty was not my friend.
 ◆ puberty [퓨버리] 사춘기

◎ The sponsors are putting me up for the night at the Four Seasons.
 ◆ the Four Seasons : 사계절이라는 뜻은 당연히 아니고요. 뒤에 이걸로 농담을 하긴 합니다만. 호텔 이름입니다. 미드에서 때때로 들립니다. 미드 30락에서는 시즌 4를 시작하면서 치아 호텔 이름으로 말장난을 합니다. 이름이 그렇잖아요. :-)
 포시즌스 호텔이 한국 서울에도 있더군요.

◎ the resentment you've been harboring for 20 years?
 ◆ harboring harbor : 마음속에 품다

◎ we need to work our butts off practicing.
 ◆ work one's butt off : 근면히 하다
 뭐 빠지게 열심하다, 시고로 식의 표현입니다. 영어도 마찬가지네요.
같은 종류로 다음과 같은 것이 있습니다.
 work one's ass off
work one's tail off
work one's head off
 우리말 '거시기 빠지게 하다'에 해당하는 영어 표현이 있는데요. 우리와 달리 여성 거시기를 씁니다. 다만 우리처럼 아마 쓰진 않는다고 하네요. 마침내 뺐습니다.

◎ I chickened out.
 ◆ chickened out : 하는 것을 두려워 하다. 그렇게 겁먹다.

◎ My hands shake when I putt. It's called the yips.
 ◆ yips [입스] 대사에 설명이 주야장천 나왔네요. 골프 퍼팅할 밥 불안한 상태를 말합니다.

◎ There'll be a nice family vibe
 ◆ vibe : 분위기. 생활영어 단어로 미국인이 자위 씁니다.
 미드 슈퍼걸 시즌1 5화 sapphic vibe
미드 필라델리피아는 백날 맑음 시즌11 1화 Mexican cholo vibe
미드 필라델리피아는 노 맑음 시즌11 3화 party vibe pathetic vibe
미드 빅뱅이론 시즌9 14화 Wait, are you saying that we didn't have a vibe?

◎ Neil deGrasse Tyson? He's a hugely popular astrophysicist.
 ◆ 두 사람의 얘기 중에서 우리나라 사람들은 똑바로 모르는 미국 유명인이 나옵니다. Kim Zolciak. Neil deGrasse Tyson.
 Neil deGrasse Tyson은 최신 미드에서 시어머니 인용되는 사람입니다.
 미드 빅뱅이론 시즌9 22화에서는 닐 디그래스 타이슨의 이름으로 말장난을 했고요.
http://hi007.tistory.com/2425
 미드 브루클린 나인나인 시즌3 9화에서는 제출물로 [골프입스](https://late-race.com/sports/post-00028.html) 본인으로 출연했습니다.
http://hi007.tistory.com/1882

◎ Let's just go first and get it over with.
 ◆ get it over with : 곧 해치우자
 미드 모던패밀리 시즌1 16화 Let's get it over with.
미드 모던패밀리 시즌1 20화 Let's just get this over with.
미드 빅뱅이론 시즌9 15화 Let's get it over with.

◎ Enough with this therapy baloney!
 ◆ baloney [쁠로니] 허튼 소리, 어리석은 짓

◎ we'd be in a world where people just blurt out
 ◆ blurt out 어쨌든 아무 허풍 가난히 말하다

◎ Whenever Ethan's around, you always get bent out of shape.
 ◆ bent out of shape : 속이 상하다. 화를 내다. 미국인이 선비 쓰는 표현입니다.
 미드 빅뱅이론 시즌9 21화 he's all bent out of shape

◎ You are a pain in the ass, Mike.
 ◆ pain in the ass : 골치덩이리. 첩경 쓰는 말입니다.

◎ Look at us getting all mushy.
 ◆ mushy [머쉬] 지나치게 감상적인

◎ Eh, Captain Crunch. With Crunchberries? It's good stuff.
 ◆ Captain Crunch : 마이크가 먹고 있는 시리얼의 이름입니다. Crunchberries라고도 부릅니다.

 By Source, Fair use, https://en.wikipedia.org/w/index.php?curid=13868903

 By BrokenSphere - Own work, CC BY-SA 3.0, https://commons.wikimedia.org/w/index.php?curid=5626144
 오리지널 Captain Crunch는 보는 것처럼 경계 가지만 들었죠. 이후에 나온 새 버전 Captain Crunch Berries는 알록달록한 Berries가 들었습니다.

 By Panatenda - Own work, CC BY-SA 3.0, https://commons.wikimedia.org/w/index.php?curid=19847420

◎ Lucky Charms has marshmallows?
 ◆ Lucky Charms : 시리얼 마크 이름입니다. 마시멜로 들었습니다. :-)


 By Sarah Mahala Photography & Makeup Artistry from Oshkosh, WI, United States - Flickr, CC BY 2.0, https://commons.wikimedia.org/w/index.php?curid=46790034
