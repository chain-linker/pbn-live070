---
layout: post
title: "(리뷰) 네이버 스마트스토어 마케팅 시작하기"
toc: true
---

  "한빛미디어 <나는 리뷰어다> 활동을 위해서 책을 제공받아 작성된 서평입니다."

## 

## 책제목 :
 초보 판매자가 빅파워셀러로 거듭나는 네이버 스마트스토어 마케팅 시작하기

## 저자 : 정진수 지음

## 출판년도 : 2021.06.28

## 책을 읽기 전에...
 예전부터 한번즘 그런생각을 한적이 있다.
 나도 쇼핑몰을?
 예전부터 친구들, 혹은 친구의 지인들, 내지 TV에서 연예인들의 쇼핑몰
 성공담을 듣다보면 자연스럽게 나도 쇼핑몰을 시도해 볼 생명 있을까 하고

 생각해 본 비교적 있다.
 막상 부러워 하기만 타격 보고 실지 도전해 보거나 시도해 휘하 않았던건
 쇼핑몰에 무지하고 실패에 대한 부담감도 정도 때문이었다.
 그래도 근시 같은 시대에 코로나로 인해 대면 영업이 힘들어 지고 비대면 거래나
 쇼핑몰, 배달업이 흥행하는 언택트 시대를 살아가고 있는 상황이다보니
 다시금 이쪽으로 관심이 생기기 시작했다.
 육아로 인해 직장을 어쩔 성명 궁핍히 이내 둬야 했던 육아맘 입장에서 재취업은 하 힘들고
 시간을 매우 뺏기다 보니 온라인 쇼핑몰에 관심이 담뿍이 갈 수명 밖에 없다.
 그럼에도 불구하고 0에서 시작할 수는 없기에 날찍 책을 통해 실패가능성을 줄이고 성공 가능성을 늘릴 운명 있는
 노하우들을 선차 살펴보았다.
 

## 책의 내용...
 이금 책의 내용은 짧게 말하면. 스마트하게 장사하자! 이라고 보면 된다.
 책의 프롤로그에 나와 있듯이 스마트 스토어를 시작하는 사람들에게

 희망과 힘을 주고, 막막함을 어느정도 해결해 주기 위한 책이다.

 

### 귀퉁이 1. 네이버 쇼핑과 스마트스토어 생태계 분석
 활시위 시대의 흐름에 대한 이야기로 시작을 한다.

 코로나 이래 언택트 시대를 맞이하면서 한 식으로 결제가 이루어 지고 있고
 네이버의 강점에 대해 일목요연하게 비교해 준다.

 스마트스토어의 장점과 수모 것들을 추가로 활용할 무망지복 있는지, SNS를 어떻게 활용하는지 설명한다.
 계통 1에서는 전반적으로 시방 상황들과 어떤것들이 있는지, 어떻게 활용하는지 등에 대해 설명한다.
 

### 갈래 2 스마트스토어 성공 전략은 기본을 잘하는 것
 파트1에서는 스마트스토어에 대해 알아보았다면 파트2에서는 창업을 위한
 여러가지 사전 준비들, 기초지식들, 기본을 다지는법, 밑바탕 전략등에 대해 다룬다.

 스마트 스토어 창업을 위한 경험을 쌓기 위해 미리 소비자로서 쇼핑해보는 방법이나
 스마트스토어를 개설해서 판매자로 등록하는법, 스마트스토어 홍보를 위해
 SNS를 시작해서 인플루언서들을 관찰하기 로 기본을 다진다.

 눈치 판매제품이 즉속히 검색되도록 구하 위해 네이버 검색엔진에 대한 이해도와
 누구 아이템이 대박아이템이 되는지, 어떻게 판매할 아이템을  선정할지에 대해 배운다.
 

### 국부 3 스마트스토어를 상위 노출하는 최고의 방법
 갈래 3에서는 파트2의 연계선상으로 스마트스토어를 상위노출하기 위한
 몇가지 전략들에 대해 말 한다.

 냄새 제품이 상위 노출되게 하절 위해 키워드, 카테고리등의 의미와 전략에 대한
 중요성과 키워드를 활용하는법, 시재 판매중인 제품을 연계해서 노출시키는 법에 대한
 내용을 다루고 있다.

 뿐만 아니라 매출이 급증하는 다양한 판매촉진 전략으로 [네이버스마트스토어](https://knowledgeable-imbibe.com/life/post-00039.html) 쿠폰과 포인트, 최저가 가격에 대한
 중요성을 설명해 주고 리뷰관리와 고객과 소통을 위한 네이버 톡톡에 대한 설명을 다룬다.

 

### 국부 4. 온라인 마케팅 최강자가 알려주는 스마트스토어 마케팅
 파트4에서는 스마트스토어 마케팅에 대한 내용이다.

 스마트스토어 초보 판매자를 위해 네이버 블로그 마케팅과

 틈새시장 공략을 위해 인스타나 페이스북 결실 같은 SNS를 활용하는 방법,
 또한 온라인 마케팅을 위한 유용한 도구로서 상길 상징 순위, 위계 추적, 키워드 제재 등
 몇가지 유용한 정보들을 다룬다.

 

## 책을 읽고나서
 책을 읽기 전까지는 온라인 쇼핑몰이 아주 멀게만 느껴졌고 막연하게만 느껴졌는데
 보탬 책을 읽고 난 사후 편시 더욱 친숙하고 쉽게 다가왔다.
 책의 내용도 무척이나 알기 쉽게 많은 도표와 그림들로 이루어져 있다 보니
 쉽고 편하게 읽을 고갱이 있었다.

 

 중간중간 핵심콕콕 Tip 부분에서 제공되는 추천 url 부분도 추절 정보를

 얻기에 더없이 유용했다.

 

 실전 재간 부분을 읽다보면 무지 흥미로운 내용이 많았고 요약정리 부분도
 중요한 부분만 다시한번 체크 피해 볼 생목숨 있어 좋았다.

 

 뿐만 아니라 내가 그동안 모르고 사용했던 스마트스토어가 이런즉 방식으로 운영되고 있다는 걸
 알고 나니 올려져 있는 제품이나 키워드, 쿠폰등을 보며 여러 생각이 든다.
 모두 꼼꼼하고 계획된 이벤트이고 세일 촉진을 위한 마케팅이며 전략이라는게
 차츰 눈에 보이기 시작한다.
 온라인 마케팅을 시작하거나 스마트스토어를 통해 마케팅을 시작하려는 사람에게는
 무척이나 유용한 책이라고 생각한다.
 

 

##  "한빛미디어 <나는 리뷰어다> 활동을 위해서 책을 제공받아

## 작성된 서평입니다."
 

 책무 인사닦음 링크 : https://www.hanbit.co.kr/store/books/look.php?p_code=B3935386244

 

