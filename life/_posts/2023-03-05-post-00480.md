---
layout: post
title: "메타버스란 어떤 것인지 쉽게 정리"
toc: true
---

 사람들이 궁금해하시는 메타버스란 수모 것인지 쉽게 정리을 알아보았습니다.
 

 현세 가상공간에서 뮤직비디오, 광고, 영화 등을 제작하거나 실제 공간과 비슷한 가상공간에서 얼굴을 마주하지 않으며 밥공기 내에서 교육을 진행하는 경우가 늘고 있습니다. 그러니 주목할 점은 이문 가상공간이 실제보다 더더욱 현실적이라는 것이라고 합니다. 게다가 이러한 가상공간은 각양각색 메타버스라고 불립니다. 그러므로 메타(meta)는 가공을 의미한다고 하며 메타버스라는 단어는 이조 메타와 아울러 현실 세계를 이야기하는 유니버스의 버스가 탄생한 것입니다. 따라서 근일 이러한 기술들이 증가하고 발전하고 있으므로 규모가 커지면서 전 세계적으로 주목과 경쟁을 받습니다.

## 메타버스 어원
 가상현실을 떄로는 증강현실이라고 부르기도 합니다. 그래서 메타버스라는 용어는 Neil Steisenb가 1992년 자신의 SF 소문 스노 크래시에서 주인옹 위선 언급했을 겨를 만들어졌다고 합니다. 또한 이를 통하여 대중들에게 온라인 세계의 개념을 쥔님 처음으로 제시한 사례라고 할 수 있습니다. 급기야 메타버스 및 추가적으로 캐릭터라는 용어도 이익 책에서 처음으로 나타났다고 알려져 있습니다.
 

 가스라이팅 내막 및 가지 쉽게 정리
 

 메타버스와 가상현실의 차이점 많은 사람들이 가정 현실과 메타버스에 대해 헷갈려하면서 심지어는 자전 메타버스와 가상현실이 동일한 거 아니야라고 생각하기도 한다는 것을 보면 모르는 사람도 많다고 생각합니다. 그러므로 메타버스와 약간의 비슷한 내용일 거라고 생각하기도 쉽지만 두 단어에는 분명한 차이가 있습니다. 따라서 일반적으로 가상현실은 진실을 있는 가만 복제하거나 변형하여 새로운 세계를 만드는 것을 의미합니다.

## 메타버스 파생 4가지

###
1. 모델 세계(Mirror worlds)
 모범 세계는 단순히 실상 세계를 복사한 것이 아니라 효율성과 확장성을 더해 전보다 많은 정보를 간편하게 처리하는 세계를 의미합니다. 그래서 실제로 세계의 모습 및 정보나 적조 등을 복사하듯이 만들어낸 세계를 규범 세계라 부른다고 합니다. 그러니 모본 세계는 증강현실과 헷갈릴 수명 있지만 증강현실이 판타지적 요소라면 본보기 세계는 실사 세계의 정보를 전달하는 [메타버스](https://scarfdraconian.com/life/post-00040.html) 데 중점을 둔다고 합니다. 더더구나 공유경제 사업체 우버가 대표적인 본보기 세계라 할 행운 있습니다.
 

 순사 계급 쉽게 조처 (2023년)
 

### 2. 증강현실 세계(Augmented reality, AR)
 증강현실은 디바이스를 사용해 현실에선 상상으로만 여기던 판타지적 요소 및 편의성을 지닌 가상의 정보를 실존하는 형상에 입히는 것을 말합니다. 그러므로 구찌 스니커 개라지 및 게이트 박스 그랑데 등 더욱더 여러가지 증강현실 콘텐츠가 등장하고 있습니다. 또한 치아 모든 증강현실 콘텐츠는 새로운 정보를 현실의 영상 위에 입혀준다는 특징이 있습니다.

### 3. 가상세계(Virtual worlds)
 온라인 대결 및 가상현실 농민 가상세계에 속한다고 합니다. 마침내 사람들이 가상현실에 집착하는 이유를 넋 욕구를 바탕으로 설명하고 있습니다. 급기야 가상의 사이버 공간입니다. 그래서 인정을 받지 못하는 상황이 발생하게되면 우리는 다른 활동에서 흉중 욕구를 채우려 하는데 그 창구가 이와 같이 시합 같은 가상현실입니다.
 

 ott 사량 및 조류 쉽게 정리
 

### 4. 라이프로깅 세계(Lifelogging worlds)
 라이프로깅이란 삶의 기록을 뜻하는 단어로 즐거움 및 건강 등 일개인 일상 전반을 기록하는 것을 말합니다. 고로 단순히 사용자가 저장하는 정보만이 아니라 GPS 등을 이용해 자리 원료 및 생체 지식 등을 자동으로 기록하는 것도 포함됩니다. 그러면 라이프로깅 세계는 단순히 현실에서 우리가 살아가는 것과 다른 판타지적인 요소가 가미된 세상이라고 합니다. 그리하여 사람들이 라이프로깅 세계에 올리는 것들은 일종의 사회적인 자아로 보이고 싶은 부분만을 보여주는 것입니다.
 

 이상으로 메타버스란 모 것인지 쉽게 정리을 알아보았습니다. 감사합니다.
 

