---
layout: post
title: "용인 푸르지오 원클러스터 1단지 청약 신청, 평면도, 분양가"
toc: true
---

 ​
 안녕하세요. 유부동산입니다!
 ​
 경기도 용인시 남동에 용인 푸르지오 원클러스터 1단지 1,681세대, 2단지 1,804세대, 3단지 239세대 규모로 대단지 아파트가 들어서는데요.
 ​
 거기 새중간 1단지 입주자 모집공고문이 나와서, 신청 일정과 방법, 특별공급·일반공급 청약 조건, 타입별 평면도, 분양가를 정리해 봤습니다.
 ​
 참고로 1순위 청약은 주택수와 도임 없이 만 19세 종말 수도권 거주자라면 누구나 청약 가능하다고 하니, 아래에서 자세한 컨디션 확인해 보시기 바랍니다.
 ​
 용인 푸르지오 원클러스터 1단지 중요 내용
 위치
 ■ 번지 : 경기도 용인시 처인구 남동 계산 126-13 일원
 경기도 용인시 처인구 남동 산126-13
 ​
 모집공고문 중요 내용
 용인 푸르지오 원클러스터 1단지 의무거주기간과 전매제한 등 모집공고 주 내용입니다.
 ​
 ​
 ​
 ■ 전매제한
 구분
 기준일
 기간
 특별공급
 시초 당첨자발표일(2024.08.22)
 6개월
 (2025.02.22까지)
 일반공급
 ​
 용인 푸르지오 원클러스터 1단지 청약 신청
 용인 푸르지오 원클러스터 1단지 신청 일정과 방법입니다. 특별공급과 일반공급별로 일정과 방법이 다르니, 아래에서 확인해 보세요!
 ​
 ​
 특별공급 신청 일정 및 방법
 구분
 내용
 신청대상자
 기관추천, 다자녀가구, 신혼부부, 노부모부양, 생애최초
 신청방법
 인터넷 : PC 내지 모바일
 현장접수 : 장애인, 고령자 등 정보취약계층에 한함
 신청일시
 2024. 8. 12 (월)
 PC 또는 모바일 : 식전 9시 ~ 오후 5시 30분
 현장접수 : 식전바람 10시 ~ 오후 2시
 신청장소
 PC 또는 모바일 :  「청약Home」 홈페이지
 현장접수 : 모델하우스 (견본주택)
 ※ 거주소 : 경기도 용인시 수지구 풍덕천동 71-1
 ​
 일반공급 신청 일정 및 방법
 구분
 내용
 신청방법
 인터넷 : PC 내지 모바일
 방문접수 : 장애인, 고령자 등 정보취약계층에 한함
 신청일시
 1순위
 2024. 8. 13 (화)
 PC 내지 모바일 : 여명 9시 ~ 오후 5시 30분
 방문접수 : 아침나절 9시 ~ 오후 4시
 2순위
 2024. 8. 14 (수)
 PC 내지 모바일 : 효두 9시 ~ 오후 5시 30분
 방문접수 : 여명 9시 ~ 오후 4시
 신청장소
 PC 또는 모바일 :  「청약Home」 홈페이지
 방문접수 : 청약통장 가입은행 창구
 ​
 용인 푸르지오 원클러스터 1단지 청약 신청 자격
 특별공급 청약 신청 자격
 용인 푸르지오 원클러스터 1단지 특별공급 계층별 청약 신청 자격입니다.
 ​
 신혼부부와 생애최초 특별공급의 소득·자산 심사 기준은 아래에 따로따로 있으니, 나란히 확인해 보세요!
 ​
 ​
 ​
 【신혼부부 소득·자산 기준】
 ■ 수입 기준
 출처 : 용인 푸르지오 원클러스터 1단지 입주자 모집공고문
 ​
 ■ 자산 기준
 출처 : 용인 푸르지오 원클러스터 1단지 입주자 모집공고문
 ​
 ​
 【생애최초 소득·자산 기준】
 ■ 소득 기준
 출처 : 용인 푸르지오 원클러스터 1단지 입주자 모집공고문
 ​
 ■ 자산 기준
 출처 : 용인 [용인 푸르지오 원클러스터](https://humiliateoatmeal.com/life/post-00118.html) 푸르지오 원클러스터 1단지 입주자 모집공고문
 ​
 일반공급 청약 신청 자격
 용인 푸르지오 원클러스터 1단지 일반공급 순위별 청약 신청 자격입니다.
 ​
 ​
 ​
 【청약통장 지역별·면적별 예치금 도척 금액】
 출처 : 용인 푸르지오 원클러스터 1단지 홈페이지
 ​
 용인 푸르지오 원클러스터 1단지 타입별 평면도, 분양가
 용인 푸르지오 원클러스터 1단지는 총 6가지 타입으로 공급됩니다. 아래에서 타입별 공급 세대 수와 평면도, 분양가를 알아보세요!
 ​
 59A타입
 ■ 방지 세대 목숨 : 총 453세대
 기관추천 특별공급 : 45세대
 다자녀가구 특별공급 : 45세대
 신혼부부 특별공급 : 81세대
 노부모부양 특별공급 : 13세대
 생애최초 특별공급 : 40세대
 일반공급 : 229세대
 ​
 ​
 ■ 평면도
 ​
 ​
 ■ 분양가
 ​
 59B타입
 ■ 지발 세대 행복 : 총 247세대
 기관추천 특별공급 : 24세대
 다자녀가구 특별공급 : 24세대
 신혼부부 특별공급 : 44세대
 노부모부양 특별공급 : 7세대
 생애최초 특별공급 : 22세대
 일반공급 : 126세대
 ​
 ​
 ■ 평면도
 ​
 ​
 ■ 분양가
 ​
 84A타입
 ■ 지불 세대 복수 : 총 560세대
 기관추천 특별공급 : 56세대
 다자녀가구 특별공급 : 56세대
 신혼부부 특별공급 : 100세대
 노부모부양 특별공급 : 16세대
 생애최초 특별공급 : 50세대
 일반공급 : 282세대
 ​
 ​
 ■ 평면도
 ​
 ​
 ■ 분양가
 ​
 84B타입
 ■ 계산 세대 명맥 : 총 269세대
 기관추천 특별공급 : 26세대
 다자녀가구 특별공급 : 26세대
 신혼부부 특별공급 : 48세대
 노부모부양 특별공급 : 8세대
 생애최초 특별공급 : 24세대
 일반공급 : 137세대
 ​
 ​
 ■ 평면도
 ​
 ​
 ■ 분양가
 ​
 84C타입
 ■ 방지 세대 요행 : 총 147세대
 기관추천 특별공급 : 14세대
 다자녀가구 특별공급 : 14세대
 신혼부부 특별공급 : 26세대
 노부모부양 특별공급 : 4세대
 생애최초 특별공급 : 13세대
 일반공급 : 76세대
 ​
 ​
 ■ 평면도
 ​
 ​
 ■ 분양가
 ​
 130A타입
 ■ 급여 세대 길운 : 총 5세대
 일반공급 : 5세대
 ​
 ​
 ■ 평면도
 ​
 ​
 ■ 분양가
 ​
 끝으로
 오늘은 용인 푸르지오 원클러스터 1단지 특별공급·일반공급 청약 신청 일정, 방법, 자격, 타입별 평면도와 분양가를 알아봤습니다.
 ​
 용인 푸르지오 원클러스터 가까스로 인근에 삼성전자와 SK하이닉스가 입주하는 반도체 클러스터 일반산업단지 예정지가 있어 수혜 아파트로 꼽히는데요.
 ​
 더더구나 다만 내에 초등학교도 함께 지어질 예정이어서, 어린 자녀를 둔 실거주 입주자라도 득 부분은 걱정거리 안하셔도 될 것 같습니다.

 ​
 요번 포스팅에서 개인적으로 아쉬운 점은 용인 푸르지오 원클러스터 불과시 주변에 아파트가 없어서 직접적인 날씨 비교를 못 해드렸는데요.
 ​
 이번 해 봄에 분양했던 '두산위브더제니스 센트럴 용인'이 반도체 클러스터 일반산업단지 효과를 얻어 계약이 서두 성사되었고, 용인지역 아파트 매매 건수도 매삭 증가세를 보이고 있어 실거주자와 투자자 수요가 계속 있는 것으로 파악됩니다.
 ​
 웅재 용인시 풍덕천동에 모델하우스가 오픈되어 있으니 관심있는 분들은 세상없어도 방문해 보시고, 저는 입주자 모집공고문 첨부해 드리면서 현금 포스팅 마치겠습니다. 감사합니다!
 ​
 ※ 용인 푸르지오 원클러스터 1단지 모델하우스 주소
 경기도 용인시 수지구 풍덕천동 43
 ​
 ​
 ▼ 용인 푸르지오 원클러스터 1단지 입주자 모집공고문
 ​
 ​
 ​
