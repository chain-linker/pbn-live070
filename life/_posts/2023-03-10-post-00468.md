---
layout: post
title: "비탈릭 부테린의 암호화폐 이더리움"
toc: true
---


## 이더리움이란?
 이더리움은 해산 원장 기술인 블록체인을 기반으로 극한 암호화폐입니다. 이더리움은 기존의 비트코인과 달리 스마트 계약을 실행할 핵심 있는 플랫폼으로 DApp 개발에 무지무지 유용한 기술입니다. 이더리움은 DApp에서 사용되는 토큰을 만들 수 있도록 지원하며, 이러한 토큰의 표준은 ERC-20이라는 이름으로 정해져 있습니다.
 이더리움의 토큰인 ETH는 이더리움의 생명줄이며, 이더리움 네트워크를 사용하려면 ETH를 이용해 수수료를 지불해야 합니다. 이더리움은 스마트 계약을 통해 다양한 산업분야에서 활용될 호운 있으며, 디지털 자산의 거래, 시합 등 다양한 분야에서 활용되고 있습니다.
 2023년 기준으로 이더리움에 대한 투자 적합성 판정은 심히 높은 수준으로 진행되고 있으며 암호화폐 시장에서 가부 중요한 플랫폼 중간 하나입니다. 더욱이 이더리움은 비트코인과 나란히 암호화폐 시장의 대표주자로 자리잡았으며 이더리움이 가지고 있는 기술력과 약진 가능성은 높게 평가되고 있습니다.
 

## 이더리움의 역사
 이더리움은 2015년에 출시된 블록체인 기반의 산출 컴퓨팅 플랫폼입니다. 이더리움은 비트코인과는 달리 스마트 컨트랙트를 실행할 고갱이 있는 플랫폼을 제공하며, 이를 통해 다양한 분야에서 활용될 행우 있습니다.
 이더리움의 역사는 2013년 Vitalik Buterin이 발표한 백서에서부터 시작됩니다. 이후 2014년 이더리움 개발자들은 펀딩을 받고 이더리움을 개발하게 되었습니다. 이더리움의 개발 과정에서는 여러 유개 기술적 문제가 있었지만, 개발자들은 이를 극복하며 2015년 이더리움을 출시하게 됩니다.
 이더리움 출시 이후, 이더리움은 빠르게 성장하며 다양한 분야에서 활용되었습니다. 2016년에는 엄청난 해킹 사건이 발생하여 이더리움의 존립이 위험한 상황이 있었지만, 개발진의 빠른 조치로 세밀히 해결되었습니다. 2017년에는 이더리움을 통한 ICO 폭증으로 이더리움 수요가 폭발적으로 증가하였고 이더리움 시세는 863달러의 최고가를 기록하기도 했습니다.
 또한, 2017년에는 크립토키티라는 DApp 기반 게임이 출시되면서 네트워크가 마비되는 사건이 있었습니다. 이익 사건은 이더리움의 가능성과 한계를 동시에 보여주었습니다.
 이더리움은 현재까지도 빠르게 발전하며 블록체인 기술을 기반으로 경계 새로운 서비스와 애플리케이션을 개발하고 있습니다.
 

## 이더리움 창시자 비탈릭 부테린
 이더리움 창시자인 비탈릭 부테린은 프로그래머이자 작가입니다. 그는 이더리움 블록체인을 개발하여 2세대 암호화폐 시대를 이끌어 냈습니다. 비탈릭 부테린은 전 세간 암호화폐와 블록체인 분야에서 주인 큰 영향력을 가지고 있으며, 암호화폐 시장이 확대되는 데 거대한 공을 세운 인물입니다. 또 요사이 이더리움 가상화폐의 강세에 힘입어 최연소 가상화폐 억만장자가 되기도 했습니다. 부테린은 2018년 10월 기준으로 이더리움 33만3520개를 보유하고 있으며 이를 달러로 환산하면 11억9600만 달러(약 1조3272억원)입니다. 부테린은 1994년 러시아에서 태어나 6살 시대 가족과 다름없이 캐나다로 이주해 토론토에서 자랐습니다.
 

## 이더리움의 장점
 암호화폐 이더리움은 비트코인과 함께 대표적인 암호화폐중 하나로 널리 알려져 있습니다. 그럼에도 이더리움은 적용범위에서 큰 차이를 보입니다. 비트코인이 결제나 통상 연관 시스템, 목금 화폐로서의 기능에 집중하는 반면, 이더리움은 추축 기술인 블록체인을 기반으로 거래나 결제뿐 아니라 계약서, SNS, 이메일, 전자투표 등 다양한 분야에서 활용될 명 있습니다. 그리고 이더리움은 암호화폐 생태계에서 중요한 역할을 합니다. 대부분의 도로 발행되는 암호화폐는 이더리움 생태계를 통해 개발되어 지고 있으며, 이더리움 생태계는 암호화폐 플랫폼이 제공하는 가치와 향후 크립토 생태계의 향방을 좌지우지할 정도로 영향력이 높습니다.
 짐짓 이더리움은 확자엉 문제를 해결하기 위해 샤딩 기술을 접목시키는 등 기술적인 발전을 이어가고 있습니다.
 이러한 이유로 암호화폐 이더리움은 화폐적 가치뿐 아니라 블록체인 기술의 구사 분야와 암호화폐 생태계에서의 역할에서도 큰 장점을 가지고 있습니다.
 

## 이더리움의 전망
 이더리움은 가상화폐와 블록체인 기술을 기반으로한 플랫폼입니다. 이더리움의 전망에 대해서는 다양한 의견이 있지만, 일반적으로 긍정적으로 평가되고 있습니다.
 이더리움은 현재도 다양한 분야에서 개발이 이루어지고 있으며 이더리움 2.0과 [암호화폐](https://seek-glow.com/life/post-00039.html) 디파이 인프라에 대한 투자가 이루어지고 있습니다. 이러한 투자는 이더리움의 안정성과 개진 가능성을 대변합니다.
 더구나 이더리움은 벌써 수 많은 블록체인과 가상화폐들이 이를 기반으로 운영되고 있는 만큼, 이더리움의 끊임없이 가능성은 의심할 여지가 없습니다. 또한 이더리움의 창시자 비탈릭 부테린은 벌써 세계적인 언설 어워드에서 수상하며 거기 업적을 인정받았습니다. 최근에는 중국의 암호화폐 거래와 ICO 금지 조치를 취소할 가능성이 있다는 기사가 보도되어 긍정적인 시선도 나오고 있습니다. 이러한 흐름을 고려할 때, 이더리움은 앞으로 더 발전하고 성장할 것으로 전망됩니다.
