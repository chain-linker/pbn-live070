---
layout: post
title: "장마철 집에서 볼 넷플릭스 한국 드라마 추천"
toc: true
---

 금번 여름엔 비가 무진히 온다고 하는데
 금차 주만 해도 대다수 일주일 어쨌든지 과실 소식이 있다. 🥺
 덥고 습해서 밖에 나가기조차 꺼려지는 요즘은
 집에서 맛있는 거 먹으면서 쉬는 게 최고!
 ​
 나는 대부분 집에서 쉴 시태 넷플릭스를 자주 애용하는데
 최근에 봤던 거 중에 재미있던 거를 추천해 보려고 한다.
 나중에 내가 재차 봐야지 >.<
 ​
 ​
 1. 환혼
 환혼은 시즌제로 방영된 tvN 드라마인데
 <환혼>, <환혼 빛과 그림자>로 나눠져 있다.
 ​
 -드라마 소개-
 역사에도 지도에도 존재하지 않은 대호국을 배경으로,

 영혼을 바꾸는 '환혼술'로 인해 운명이 비틀린 주인공들이

 이를 극복하고 성장해가는 판타지 정교 활극
 ​
 환혼은
 진무(조재윤)에 의해 살수로 자라게 된 낙수(정소민)가 반대세력인 송림과 싸우던 가운데 죽을 위기에 처하게 되고 살아남기 위해 무덕이의 몸으로 환혼을 하게 되면서 시작하는 [링크나라](https://quarrel-sleepy.com/entertain/post-00048.html) 이야기이다.
 대단히 약한 몸으로 들어간 탓인지 낙수는 자초 가진 힘을 하나도 사용하지 못하게 된다.
 또한 대호국에서 장욱(이재욱)을 마주치게 되는데… 장욱은 아버지인 장강에 의해 태어나자마자
 기문이 막히게 되고 다른 술사들과 다르게 아무아무 힘을 사용하지 못하고 억지로 평범하게 살아가는 중이었지만 재삼재사 돌아가신 아버지처럼 천부관 최고의 술사가 되고 싶어 했다.

 장욱은 우연히 마주치게 된 무덕이의 눈에서 환혼인의 푸른 자국을 보게 되고 살수인 낙수가 환혼을 했다는 사실이 떠오르게 되면서 자연스럽게 무덕이가 낙수인 사실을 알아차리게 된다.
 장욱은 대호국에서 자신을 가르쳐 줄 스승은 낙수밖에 없다고 생각했고 무덕이=낙수를 스승으로 모시게 된다. 장욱은 환혼인인 무덕이를 지켜주고 무덕이는 장욱의 기문을 뚫어주는 걸로 거래를 하게 된다.
 ​
 ​
 감상평
 처음엔 설정만 보고 이렇게 유치한 드라마가 있다고? 생각을 했는데
 이렁저렁 환며들어있는 나를 보게 되었다 ..
 판타지물이고 액션이 들어가다 보니 지루하지 않고 영속 보게 되는 매력이 있고
 다양한 캐릭터들의 설정과 능력이 재미있다.
 이재욱과 정소민의 연기 케미가 아주 좋았고 나중에는 둘의 이루어질 행복 없는 사랑이
 진실 마음이 찢어진다 ..ㅠㅠ..ㅠㅠㅠㅠ
 시즌제로 되어있고 아주 몰입력이 좋은 드라마기 때문에 긴 장마철에 지루하지 않게 볼 생목숨 있다.
 실제로 사방 친구들한테 영업했을 철기 반응이 남편 좋았던 드라마였다!
 ​
 2. 런 온

 분위기 최애 드라마인 런온  Run on
 개인적으로 임시완이 나온 드라마, 영화는 십중팔구 좋아하는데
 그중에서도 독 상천 어울리는 캐릭터가 아니었나 생각이 든다.
 ​
 런 온은
 단거리 육상 국가대표인 기선겸(임시완)과 외화 번역가인 오미주(신세경)의 연애 이야기이다.
 둘의 첫 만남은 영화제이다. 오미주가 달려가다가 기선겸과 부딪히면서 방향 속에 있던 총 모양의 라이터를 떨어트리게 된다. 입때껏 한석원(오미주 전남친)이 억지로 끌고 가려고 하는 모습을 보고 약속 폭력으로 착각한 기선겸이 총으로 한석원을 겨누게 되면서 만나게 된다.
 기선겸은 늘 국회의원 아버지, 탑 배우인 어머니, 골프 여제인 누나의 밑에서 자랐고 육상 국가대표 기선겸이 아닌 누구의 아들, 동생으로 항상 불리게 된다. 가족의 그늘에서 자신은 응당 가려진, 답답하기만 테두리 삶을 살아왔다.

 오미주는 어린 기후 부모를 여의고 독자 자랐지만 자신을 불쌍하게 생각하는 사람을 이해할 복수 없을 만큼 밝게 자라온 사람이다.
 ​
 항상 앞만 보고 달리던 기선겸과, 번역가의 특성상 부절 뒤를 돌아보며 살아가는 오미주
 같은 한국어를 반대로 왠지 모르게 다른 대화를 하는 둘의 사바세계 이야기
 명대사도 워낙 상당히 나온다.
 호위호 실패를 계서 안에 중 끼워주지?
 실패하는 것도 완성을 향해 달려가는 과정에 포함을 시켜줘야죠.
 ​
 극복이라는 게 확실히 매 별안간 일어나야 하는 건 아니에요
 주말엔 쉬어도 돼
 그러니깐 여름철 싫은 건 다리파 마요 그게 뭐든.
 어딘가에 지쳐있는 사람들이라면 기연 보길 추천하는 드라마
 보면서 위로를 굉장히 받게 된 드라마이다.
 모든 캐릭터들이 적성 있고 티키타카가 올바로 되기 그렇게 재미도 있다.
 ​
 서비스 커플인 서단아(최수영)과 이영화(강태오)의 사랑도 빼먹을 길운 없는 관전 포인트.
 ​
 3. 사랑의이해
 보고 나서 바깥주인 많은 여운이 남은 드라마인 사랑의 이해
 서점에서 대본집을 판매하는 걸 보고 구매할까 그저 염려 무지 했다.
 사랑에 대한 모든 감정을 나타낸 드라마.
 ​
 4명은 모두 같은 은행에서 만나게 된다.
 안수영(문가영)과 하상수(유연석)가 서로에게 호감을 가지다가 오해로 틀어지게 되면서
 안수영은 정종현(정가람)과 만나게 되고, 하상수는 박미경(금새록)과 만나게 된다.
 도리어 안수영과 하상수는 마음속에서 서로를 포기하지 못하게 된다 …
 ​
 ​
 오랜 고시생활을 하고 있는 정종현에게 곰비임비 맞춰주게 되는 안수영.
 고시원에서 나와 안수영의 집에서 고시생활을 하게 되고, 금전적으로 힘들 동안 내리내리 도와주는 안수영에게
 시나브로 자존감이 낮아지고 자기 와중에 시험에서도 속박 떨어지게 된다.
 안수영의 사랑은 차차로 알 핵 없는 동정심이 되어가고 사랑도 공부도 제 여의히 되지 않자 거개 포기하고 내려놓는 정종현을 위해 결국은 매몰차게 헤어지는 방법을 선택한다.
 ​
 하상수는 자기가 좋아하진 않지만 자신을 좋아해 주는 박미경의 적극적인 고백을 받아들이게 되고 만나다 보면 좋아지겠지 싶어서 기어코 사귀게 된다. 박미경은 부유한 집안에서 자라 금전적으로 매양 여유롭게 살아왔다. 하상수에게 내리 물질적으로 사랑을 표현하게 되는데 이를 부담스러워하고 알 수 없는 자격지심과 열등감을 가지게 된다. 박미경은 자신과 시나브로 더 멀어져 가는 모습을 이해하지 못하게 되고 종내 둘은 차츰차츰 갈등이 생기게 된다. 사실상 박미경은 본인의 아버지와 닮아있었는데 본인을 밤낮 한심하게 생각했던 아버지도 자신에게 사랑을 물질적으로 표현하고 있었다는 사실을 뒤늦게 깨닫게 된다.

 ​
 뿐만 아니라 서로에게 연인이 있지만 최종 하상수와 안수영은 서로에게 끌리게 되는 내용이다.
 ​
 행복, 하고 있어요?
 헤어질까요?
 사랑을 표현하는 방법과 마음이 사람마다 다른 것을 걸핏하면 보여주는 드라마이고
 잘못된 관계라는 걸 알면서 놓지 못하는 마음이 한 번쯤은 다들 느껴본 감정이 아닐까 싶다.
 사랑함으로써 느낄 운명 있는 비참함을 잘 느낄 행운 있던 것 같다.
 ​
 4. 나의 해방일지
 ​
 번번이 반복되는 똑같은 섭세 속에서 답답함을 느끼고 있다면
 추천하는 드라마.
 ​
 하여 영실 좋은 일은 나한테만 일어나나 싶을 때도 있고
 각별히 불행하지 않지만 삶이 지루하고 재미없을 때도 있다.
 이럴 때는 뭔가 도망치고 싶은 마음이 크게 들게 되는데 …
 삼 남매는 세세히 회사에서, 연인에게서, 내절로 자신에게서 느껴지는 답답함에 한계를 느끼고
 각자의 삶에서 해방하게 된다.
 ​
 ​
 희곡 중에서 염미정(김지원)이 신분조차 단판 알 행운 없는 알콜중독자인 구씨(손석구)에게 즉 자신을 추앙하라고 얘기하게 된다. 자신의 결핍을 채우고 싶어 하는 염미정의 마지막 발악처럼 느껴졌다.
 인간관계에 지친 사람들이 보면 좋을 것 같은 드라마.
 우울한 사람과 우울한 사람이 만나서 나타내는 시너지가 좋았다고 해야 하나 ..?

 ​
 모든 관계가
 노동이에요.
 관계에 지치지만 마지막 사랑이라는 힘이 나를 해방시켜준다는 느낌을 받았던 드라마이다.
 잔잔한 느낌의 드라마지만 살림 하나하나가 명대사 일만큼 한동안 여운이 워낙 남았다.
 위로받고 싶다면 한번은 필연코 보시길!
 ​
 ​
 ​
